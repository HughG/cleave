import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

plugins {
    kotlin("jvm") version "1.4.0-rc"
}

group = "org.tameter"
version = "0.0.1"

repositories {
    maven { url = uri("https://dl.bintray.com/kotlin/kotlin-eap") }
    maven { url = uri("https://jitpack.io") }
    mavenCentral()
    jcenter()
}

tasks.compileKotlin {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

val grafanaCompile by configurations.creating

dependencies {
    implementation(kotlin("stdlib"))
    grafanaCompile(kotlin("script-runtime"))
    implementation(kotlin("script-runtime"))
    grafanaCompile("com.yandex.money.tech:grafana-dashboard-dsl:2.19.0")
    implementation( "org.json:json:20180130")
    implementation("com.github.jillesvangurp:es-kotlin-wrapper-client:1.0-X-Beta-8-7.8.1")
}

sourceSets {
    create("grafana").withConvention(KotlinSourceSet::class) {
    }
}
