package esclient

// given some model class with two fields
data class Thing(
    val name: String,
    val amount: Long = 42
)