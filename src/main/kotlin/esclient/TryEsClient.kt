package esclient

import io.inbot.eskotlinwrapper.FieldMapping
import io.inbot.eskotlinwrapper.FieldMappings
import org.elasticsearch.client.configure
import org.elasticsearch.client.create
import org.elasticsearch.client.indexRepository
import kotlin.reflect.KProperty
import kotlin.reflect.full.declaredMembers

fun tryEsClient() {
    val esClient = create(host = "localhost", port = 9999)

    // create a Repository
    // with the default jackson model reader and writer
    // (you can use something else)
    val thingRepository = esClient.indexRepository<Thing>(
        index = "things",
        // you have to opt in to refreshes, bad idea to refresh in production code
        refreshAllowed = true
    )

    // let the Repository create the index with the specified mappings & settings
    thingRepository.createIndex {
        configure {
            // we use our settings DSL here
            // you can also use multi line strings with JSON in a source block
            settings {
                replicas = 0
                shards = 2

                addTokenizer("autocomplete") {
                    // the DSL does not cover everything but it's a proper
                    // map so you can fall back to adding things directly
                    this["type"] = "edge_ngram"
                    this["min_gram"] = 2
                    this["max_gram"] = 10
                    this["token_chars"] = listOf("letter")
                }
                addAnalyzer("autocomplete") {
                    this["tokenizer"] = "autocomplete"
                    this["filter"] = listOf("lowercase")
                }
                addAnalyzer("autocomplete_search") {
                    this["tokenizer"] = "lowercase"
                }
            }
            // the mapping DSL is a bit more fully featured
            mappings {
                // mappings DSL, most common field types are supported
                text(Thing::name.name) {
                    fields {
                        // lets also add name.raw field
                        keyword("raw")
                        // and a name.autocomplete field for auto complete
                        text("autocomplete") {
                            analyzer = "autocomplete"
                            searchAnalyzer = "autocomplete_search"
                        }
                    }
                }
                // floats, longs, doubles, etc. should just work
                number<Long>(Thing::amount.name)

                checkMappings<Thing>()
            }

            throw RuntimeException("Stop!")
        }
    }

    /*

    // lets put some things in our new index
    thingRepository.index("1", esclient.Thing("foo", 42))
    thingRepository.index("2", esclient.Thing("bar", 42))
    thingRepository.index("3", esclient.Thing("foobar", 42))

    // make sure ES commits the changes so we can search
    thingRepository.refresh()

    // putting things into an index 1 by 1 is not scalable
    // lets do some bulk inserts with the Bulk DSL
    thingRepository.bulk {
        // we are passed a BulkIndexingSession<esclient.Thing> in the block as 'this'

        // we will bulk re-index the objects we already added with
        // a scrolling search. Scrolling searches work just
        // like normal searches (except they are not ranked)
        // all you do is set scrolling to true and you can
        // scroll through billions of results.
        thingRepository.search(scrolling = true) {
            // A simple example of using the Kotlin Query DSL
            // you can also use a source block with multi line JSON
            dsl {
                from = 0
                // when scrolling, this is the scroll page size
                resultSize = 10
                query = bool {
                    should(
                        MatchQuery("name", "foo"),
                        MatchQuery("name", "bar"),
                        MatchQuery("name", "foobar")
                    )
                }
            }
        }.hits.forEach { (esResult, deserialized) ->
            // we get a lazy sequence with deserialized esclient.Thing objects back
            // because it's a scrolling search, we fetch pages with results
            // as you consume the sequence.
            if (deserialized != null) {
                // de-serialized may be null if we disable source on the mapping
                // uses the BulkIndexingSession to add a transformed version
                // of the original thing
                index(
                    esResult.id, deserialized.copy(amount = deserialized.amount + 1),
                    // to allow updates of existing things
                    create = false
                )
            }
        }
    }

    */
}

// NOTE 2020-08-14 hgreene: This is the only thing I added to the above demo code from the project README.
//
// The idea is that, for a given data class, it will reflectively check that each property has a sensible mapping.  It
// doesn't do that yet, it just prints out the correspondence between properties and the field mapping, but it gives you
// an idea how you could do it.  If you comment out the mapping for the "Thing::amount" field, for example, this will#
// output "..., mapping type null".
//
// You could then correspondingly check your logstash filters against your ES mapping by configuring logstash to output
// events as JSON, then use some kind of JSON deserialisation to check that they all match the expected Kotlin data
// class, in terms of fields present and their types.  (You might need some extra logstash config if you also want to
// check common metadata like "@timestamp".  In that case you might inherit those from another class (and not just use
// a data class) and then you'd also want to check against "members" below, rather than just "declaredMembers".
private inline fun <reified T> FieldMappings.checkMappings() {
    T::class.declaredMembers.filterIsInstance<KProperty<*>>().forEach { property ->
        val name = property.name
        val fieldMapping = this[name] as? FieldMapping
        println("Property $name, type ${property.returnType}, mapping type ${fieldMapping?.type}")
    }
}