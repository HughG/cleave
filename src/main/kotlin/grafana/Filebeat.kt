package grafana

import ru.yandex.money.tools.grafana.dsl.datasource.Datasource

object Filebeat : Datasource {
    override fun asDatasourceName() = "Filebeat"
}