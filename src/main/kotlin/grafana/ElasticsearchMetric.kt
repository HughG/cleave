package grafana

import ru.yandex.money.tools.grafana.dsl.json.emptyJsonArray
import ru.yandex.money.tools.grafana.dsl.json.jsonObject
import ru.yandex.money.tools.grafana.dsl.metrics.DashboardMetric

class ElasticsearchMetric private constructor(
    private val group: String = "",
    private val application: String = "",
    private val host: String = "",
    private val item: String = "",
    private val showDisabledItems: Boolean = false,
    private val useCaptureGroups: Boolean = false
) : DashboardMetric {

    override fun toJson() = jsonObject {
        "group" to jsonObject {
            "filter" to group
        }
        "application" to jsonObject {
            "filter" to application
        }
        "host" to jsonObject {
            "filter" to host
        }
        "item" to jsonObject {
            "filter" to item
        }
        "functions" to emptyJsonArray()
        "options" to jsonObject {
            "showDisabledItems" to showDisabledItems
        }
        "useCaptureGroups" to useCaptureGroups
    }
}