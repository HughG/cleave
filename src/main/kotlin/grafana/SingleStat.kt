package grafana

import ru.yandex.money.tools.grafana.dsl.json.jsonObject
import ru.yandex.money.tools.grafana.dsl.json.set
import ru.yandex.money.tools.grafana.dsl.metrics.functions.*
import ru.yandex.money.tools.grafana.dsl.panels.PanelContainerBuilder
import ru.yandex.money.tools.grafana.dsl.panels.metricPanel

fun PanelContainerBuilder.singleStat(title: String, metric: String) {
    metricPanel(title = title) {
        datasource = Filebeat

        /*
            Panel size, 6 will be quarter of screen, as grafana has 24 columns,
            and 3 will be 90px, as height is measured in 30px intervals
         */
        bounds = 6 to 3

        properties {
            // Override some properties in JSON
            it["type"] = "singlestat"
            it["nullPointMode"] = "connected"
            it["valueName"] = "total"
            it["sparkline"] = jsonObject {
                "show" to true
                "full" to false
                "fillColor" to "rgba(31, 118, 189, 0.18)"
                "lineColor" to "rgb(31, 120, 193)"
            }
            it["colorBackground"] = true
//            it["colorValue"] = false
        }

        metrics {
            metric("A") {
                // Define a metric referencing above one (referenceId must be unique for panel)
                StringMetric(metric)
            }

            metric("B") {
                "*.another.metric.mean"
                    .groupByNodes(function = "max", 0)
                    .consolidateBy(ConsolidationFunction.MAX)
                    .averageSeries() // show average value for metric
                    .alias("another metric") // define alias
                    .perSecond() // show metric on value per second
                    .sortByTotal() // sort metrics in descending order by the sum of values across time period
            }
        }
    }
}