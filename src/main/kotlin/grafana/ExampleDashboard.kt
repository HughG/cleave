package grafana

import ru.yandex.money.tools.grafana.dsl.dashboard

val dashboardJson = dashboard(title = "My custom dashboard") {
    panels {
        singleStat("Launch Time > 10 sec", "foo.bar.baz")
    }
}