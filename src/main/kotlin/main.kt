import esclient.tryEsClient
import grafana.dashboardJson
import org.json.JSONObject
import kotlin.system.exitProcess

private fun prettyPrintJson(json: String) = JSONObject(json).toString(2)

fun main() {
    try {
        tryEsClient()
    } catch (e: Exception) {
        println("Exception: ${e}")
        exitProcess(1)
    }

//    println(prettyPrintJson(dashboardJson))
//    println(prettyPrintJson(demoLayouts))
}